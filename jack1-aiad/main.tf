provider "aws" {
  profile = "iamadmin-general"
  region  = "us-east-1"
}

resource "aws_instance" "copsrelay7" {

    ami           = "ami-0affd4508a5d2481b"
    instance_type = "t3.medium"
    availability_zone            = "us-east-1c"
    associate_public_ip_address  = true
    disable_api_termination      = "true"
    ebs_optimized                = true
    key_name      = "jackvish"
    ipv6_addresses               = []
    monitoring                   = false
    source_dest_check            = true
    subnet_id     = "subnet-0561871a33b6230ef"
    tags          = {
        "cops_type" = "mh"
        "cops_live" = "launched"
        "cops_env" = "admp1-aiad"
        "cops_group" = "t"
        "cops_infra" = "fb,mb,mp"
        "cops_hostname" = "admt1-aiad-mh01"
        "Name" = "admp1-aiad-mh01"
    }
    tenancy                      = "default"
    vpc_security_group_ids       = [
        "sg-0f94b6a6e0fe95705",
        "sg-0db3f6c0cdf99ff38",
    ]
    root_block_device {
        delete_on_termination = true
        encrypted             = false
        volume_size           = 100
        volume_type           = "standard"
    }

    timeouts {}
    lifecycle {
        ignore_changes = [
            user_data
        ]
    }
 }
